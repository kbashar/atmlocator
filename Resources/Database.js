function getBankTableName(bank_name)	{
	var table_name = '';
	bank_name = bank_name.toUpperCase();
	switch (bank_name) {
		case 'DUTCH BANGLA BANK':
		     table_name ='dbbl_bank';
		     break;
		case 'AB BANK':
		     table_name ='ab_bank';
		     break;
		case 'BRAC BANK':
		     table_name ='brac_bank';
		     break;
		case 'PRIME BANK':
		     table_name ='prime_bank';
		     break;
		case 'SONALI BANK':
		     table_name ='sonali_bank';
		     break;
		case 'TRUST BANK':
		     table_name ='trust_bank';
		     break;
		default:
			break;
	}
	return table_name;
}

var searchForArea = function(dict)	{
	var db = Titanium.Database.install('atmlocator.db','database_atm');
	var data = [];
	if (!dict.area)	{
		return data;
	} else {
		var resultSet = db.execute('Select DISTINCT atm_area from '+getBankTableName(dict.bankName) +' where atm_area like \''+dict.area+'%\'');
		while(resultSet.isValidRow())	{
			var area_name = resultSet.fieldByName('atm_area');
			Ti.API.info(area_name);
			data.push(area_name);
			resultSet.next();
		}
		resultSet.close();
		db.close();
		return data;
	}
};

 var searchForATMBooths = function(dict)	{
 	var db = Titanium.Database.install('atmlocator.db','database_atmr');
	var data = [];
	if (!dict.selectedArea)	{
		return data;
	} else {
		var resultSet = db.execute('Select atm_name,atm_address,atm_area,atm_district from '+getBankTableName(dict.bankName) +' where atm_area = \''+dict.selectedArea+'\' COLLATE NOCASE');
		while(resultSet.isValidRow())	{
			Ti.API.info(
				resultSet.fieldByName('atm_name')+
				"...."+
				resultSet.fieldByName('atm_address')+
				"...."+
				resultSet.fieldByName('atm_area'));
			resultSet.next();
		}
		resultSet.close();
		db.close();
		return data;
	}	
};

var getAllArea = function(dict) {
	var db = Titanium.Database.install('atmlocator.db','database_atm');
	var data = [];
		var resultSet = db.execute('Select DISTINCT atm_area from '+getBankTableName(dict.bankName)+' where atm_district = \''+dict.district+'\' COLLATE NOCASE');
		Ti.API.info('Select DISTINCT atm_area from '+getBankTableName(dict.bankName)+' where atm_district like \''+dict.district+'%\' COLLATE NOCASE');
		while(resultSet.isValidRow())	{
			var area_name = resultSet.fieldByName('atm_area');
			Ti.API.info(area_name);
			data.push({name:area_name});
			resultSet.next();
		}
		resultSet.close();
		db.close();
		return data;
};

var getAllATMBooths = function(dict)	{
	var db = Ti.Database.install('atmlocator.db','database_atm');
	var data = [];
	
	var resultSet = db.execute('select * from ' + getBankTableName(dict.bankName) + ' where atm_area = \'' + dict.area_name + '\' COLLATE NOCASE');
	while (resultSet.isValidRow()) {
		data.push({
			name : resultSet.fieldByName('atm_name'),
			address: resultSet.fieldByName('atm_address')
		});
		resultSet.next();
	}
	resultSet.close();
	db.close();
	return data;
};

var getAllDistricts = function(dict)	{
	var db = Ti.Database.install('atmlocator.db','database_atm');
	var data = [];
	
	var resultSet = db.execute('select DISTINCT atm_district from ' + getBankTableName(dict.bankName));
	while (resultSet.isValidRow()) {
		data.push({
			name : resultSet.fieldByName('atm_district'),
		});
		resultSet.next();
	}
	resultSet.close();
	db.close();
	return data;
};

var searchATMBooths = function(dict)	{
	var db = Ti.Database.install('atmlocator.db','database_atm');
	var data = [];
	Titanium.API.info(dict.bankName +"....."+getBankTableName(dict.bankName)+"......"+dict.address);
	Titanium.API.info('select * from ' + getBankTableName(dict.bankName) + ' where atm_area = \'' + dict.area_name + '\' and atm_address like \'%'+dict.address+'%\' COLLATE NOCASE');
	var resultSet = db.execute('select * from ' + getBankTableName(dict.bankName) + ' where atm_area = \'' + dict.area_name + '\' and atm_address like \'%'+dict.address+'%\' COLLATE NOCASE');
	while (resultSet.isValidRow()) {
		data.push({
			name : resultSet.fieldByName('atm_name'),
			address: resultSet.fieldByName('atm_address')
		});
		resultSet.next();
	}
	resultSet.close();
	db.close();
	return data;
};


module.exports.SearchForArea = searchForArea;
module.exports.SearchForATMBooths = searchForATMBooths;
module.exports.GetAllArea = getAllArea;
module.exports.GetAllATMBooths = getAllATMBooths;
module.exports.SearchATMBooths = searchATMBooths;
module.exports.GetAllDistricts = getAllDistricts;