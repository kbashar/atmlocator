var temp = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
var modalWin = function(dict)	{
	var popupWin = Titanium.UI.createWindow({
		modal: true,
		navBarHidden:true
	});
	
	var transparentView = Ti.UI.createView({
		backgroundColor: 'black',
		opacity: 0.4,
		height: '100%',
		width:'100%',
		id: 'transparentView'
	});
	
	popupWin.add(transparentView);
	
	var popupView = Titanium.UI.createView({
		width: 300,
		height: 350,
		backgroundColor: '#013447',
		borderRadius: 5,
		bordarColor: '#33B4E5',
		bordarWidth: 3,
		opacity: 1.0,
		layout: 'vertical',
		zIndex:1
		});
	
	/*
	 *  upper view  for pop up window
	 */
	
	var header_view = Ti.UI.createView({
		width: '100%',
		height: '18%',
		backgroundColor: '#33B4E5',
		top:0
	});
	Ti.API.info(dict.bankName);
	var headerTitle = Ti.UI.createLabel({
		width:'100%',
		height: '100%',
		top: 1,
		textAlign: Titanium.UI.TEXT_ALIGNMENT_CENTER,
		text: (dict.bankName)? dict.bankName :'Bank Name',
		font : {
		fontSize : temp,
		fontFamily : 'Arial',
		fontWeight : 'bold'
	},
		color: '#ffffff'
	});
	header_view.add(headerTitle);
	
	popupView.add(header_view);
	
	var address = dict.address.replace(' ','\n');
	
	popupView.add(
		Ti.UI.createLabel({
			width: '100%',
			height: 'auto',
	     	text: (address) ? address: 'This is Modal Window',
			font: {fontSize: 20,fontWeight: 'bold'},
			textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER
		})
	);
	
	
	popupWin.add(popupView);	
	transparentView.addEventListener('click',function(e){
		if(e.source.id === 'transparentView')	{
			popupWin.close();
		}
	});
return popupWin;
};

module.exports.ModalWindow = modalWin;
