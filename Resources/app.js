var NavigationController = require('NavigationController').NavigationController;
var controller = new NavigationController();

var temp = (Titanium.Platform.displayCaps.platformHeight * 3) / 100;
var temp1 = (Titanium.Platform.displayCaps.platformHeight*2) / 100;

var selectedBankIndex = -1;
var selectedDistrict = '';
var selectedArea = '';
var selectedAddress = '';

var window = Ti.UI.createWindow({
	backgroundColor : 'white',
	layout : 'vartical'
});

/*
 * upper portion of window
 */
var upper_portion_view = Ti.UI.createView({
	top : 0,
	backgroundColor : '#B2E6FA',
	borderColor : '#045979',
	borderWidth : 1,
	height : 150
});

var bank_title_banner = Ti.UI.createLabel({
	top : 0,
	backgroundColor : 'black',
	opacity : 0.5,
	height : 35,
	text : 'Title of bank',
	font : {
		fontSize : temp,
		fontFamily : 'Arial',
		fontWeight : 'bold'
	},
	color : '#33B4E5',
	textAlign : Titanium.UI.TEXT_ALIGNMENT_CENTER,
	width : Titanium.UI.FILL
});

var bank_logo_container = Ti.UI.createView({
	layout : 'horizontal',
	top : 0,
	horizontalWrap : false
});

var size = 100;

var addBank = function(dict) {
	var bank_view = Ti.UI.createView({
		backgroundColor : (dict.color) ? dict.color : 'red',
		height : size,
		width : size,
		left : (dict.left) ? dict.left : 5,
		right : (dict.right) ? dict.right : 10,
		focusable : true,
		borderColor : '#045979',
		bankName : dict.bankName,
		bankIndex : dict.bankIndex,
	});
	if(dict.backgroundImage) bank_view.setBackgroundImage(dict.backgroundImage);
	return bank_view;
};

var banks = [];

banks.push(addBank({
	color : 'white',
	left : 10,
	right : 10,
	bankName : 'Dutch Bangla Bank',
	bankIndex : 0,
	backgroundImage: '/images/dbbl.png'
}));
banks.push(addBank({
	color : 'white',
	left : 5,
	right : 10,
	bankName : 'Brac Bank',
	bankIndex : 1,
	backgroundImage: '/images/brac.png'
}));
banks.push(addBank({
	color : 'white',
	left : 5,
	right : 10,
	bankName : 'AB Bank',
	bankIndex : 2,
	backgroundImage: '/images/ab.png'
}));
banks.push(addBank({
	color : 'white',
	left : 5,
	right : 10,
	bankName : 'Trust Bank',
	bankIndex : 3,
	backgroundImage: '/images/trust.png'
}));
banks.push(addBank({
	color : 'white',
	left : 5,
	right : 10,
	bankName : 'Sonali bank',
	bankIndex : 4,
	backgroundImage:'/images/sonali.png'
}));

var setDataForBank = function(bank_view, _bankIndex) {
	    if (selectedBankIndex != _bankIndex) 
	    {bank_view.setBorderWidth(10);
		bank_title_banner.setText(bank_view.bankName);
		if(selectedBankIndex>=0) banks[selectedBankIndex].setBorderWidth(0);
		selectedBankIndex = _bankIndex;}
		populateTable({
			tableType : 'district'
		});
};

for (var i = 0; i < banks.length; i++) {
	banks[i].addEventListener('click', function(e) {
		var viewSelected = e.source;
		setDataForBank(viewSelected,e.source.bankIndex);
	});
	bank_logo_container.add(banks[i]);

};

var scrollView = Ti.UI.createScrollView({
	top : 0,
	scrollType : 'horizontal'
});

scrollView.add(bank_logo_container);
upper_portion_view.add(scrollView);
upper_portion_view.add(bank_title_banner);
/*
 *  middle portion
 */
var middle_portion_view = Ti.UI.createView({
	backgroundColor : '#33B4E5',
	layout: 'horizontal',
	top : 151,
	height : 45
});

var district_label = Ti.UI.createLabel({
	color: 'white',
	textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
	height:'100%',
	top: 1,
	backgroundSelectedColor: '#045979',
	font : {
		fontSize : temp1,
		fontFamily : 'Arial',
		fontWeight : 'bold'
	}
});

var area_label = Ti.UI.createLabel({
	color: 'white',
	top: 1,
	textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
	height:'100%',
	backgroundSelectedColor: '#045979',
	font : {
		fontSize : temp1,
		fontFamily : 'Arial',
		fontWeight : 'bold'
	}
});
var address_label = Ti.UI.createLabel({
	color: 'white',
	top: 1,
	textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
	height:'100%',
	font : {
		fontSize : temp1,
		fontFamily : 'Arial',
		fontWeight : 'bold'
	}
});

middle_portion_view.add(district_label);
middle_portion_view.add(area_label);
middle_portion_view.add(address_label);

/*
 * Lower portion of window. Table view for showing data
 */

var lower_portion_view = Ti.UI.createView({
	top : 198,
	bottom : 0,
	backgroundColor: '#68CBF1'
});

//Table view showing your autocomplete values
var tableview = Ti.UI.createTableView({
	height : '100%',
	width : '100%',
	backgroundColor : '#EFEFEF',
	separatorColor:'#045979',
	allowSelection : true,
	layout : 'vertical'
});
tableview.addEventListener('click', function(e) {
	selectedDistrict = e.rowData.result.name;
	Ti.API.info(selectedDistrict);
	populateTable({
		district : selectedDistrict,
		tableType : 'area'
	});
});
var tableview_area = Ti.UI.createTableView({
	height : '100%',
	width : '100%',
	separatorColor:'#045979',
	backgroundColor : '#EFEFEF',
	allowSelection : true,
	layout : 'vertical'
});
tableview_area.addEventListener('click', function(e) {
	selectedArea = e.rowData.result.name;
	Ti.API.info(selectedArea);
	populateTable({
		area : selectedArea,
		district : selectedDistrict,
		tableType : 'address'
	});
});

var tableview_address = Ti.UI.createTableView({
	height : '100%',
	width : '100%',
	separatorColor:'#045979',
	backgroundColor : '#EFEFEF',
	allowSelection : true,
	layout : 'vertical'
});
tableview_address.addEventListener('click', function(e) {
	selectedAddress = e.rowData.result.name;
	Ti.API.info(selectedAddress);
	var modalWin = require('ModalWindow').ModalWindow({
		address : e.rowData.result.address,
		bankName: banks[selectedBankIndex].bankName
	});
	Ti.API.info(banks[selectedBankIndex].bankName);
	controller.open(modalWin);
});

var populateTable = function(dict) {
	var db = require('Database');
	switch (dict.tableType) {
	case 'district':
		var districts = db.GetAllDistricts({
			bankName : banks[selectedBankIndex].bankName
		});
		
		district_label.setText('Districts');
		area_label.setFocusable(false);
		area_label.hide();
		address_label.hide();
		
		setDataOnTable({
			data : districts,
			tableType : dict.tableType
		});
		break;
	case 'area':
		var areas = db.GetAllArea({
			bankName : banks[selectedBankIndex].bankName,
			district : dict.district
		});
		
		district_label.setText(dict.district);
		area_label.setText(' < Areas');
		district_label.setFocusable(true);
		area_label.show();
		address_label.hide();
						
		setDataOnTable({
			data : areas,
			tableType : dict.tableType
		});
		break;
	case 'address':
		var addresses = db.GetAllATMBooths({
			bankName : banks[selectedBankIndex].bankName,
			district : dict.district,
			area_name : dict.area
		});
		
		area_label.setText(' < '+dict.area);
		address_label.setText(' < ATM Booths');
		area_label.setFocusable(true);
		address_label.show();
		
		setDataOnTable({
			data : addresses,
			tableType : dict.tableType
		});
		break;
	default:
		break;
	}
};

district_label.addEventListener('click',function(e){
	populateTable({
		tableType: 'district'
	});
});
area_label.addEventListener('click',function(e){
	populateTable({
		tableType: 'area',
		district: selectedDistrict
	});
});

//setting the tableview values
function setDataOnTable(dict) {
	var tableData = [];
	var searchResults = dict.data;
	Ti.API.info(searchResults.length);
	for (var index = 0,
	    len = searchResults.length; index < len; index++) {

		var lblSearchResult = Ti.UI.createLabel({
			top : 2,
			// backgroundColor : '#0899D0',
			// borderRadius: 5,
			// borderWidth: 5,
			// borderColor:'#045979',
			// left:10,
			// right:20,
			textAlign:Titanium.UI.TEXT_ALIGNMENT_CENTER,
			font : {
				fontSize : 20, 
			},
			color : '#000000',
			text : searchResults[index].name
		});

		//Creating the table view row
		var row = Ti.UI.createTableViewRow({
			focusable : true,
			height : 50,
			result : searchResults[index]
		});
		row.add(lblSearchResult);
		tableData.push(row);
	}
	switch (dict.tableType) {
	case 'district':
		if (lower_portion_view.children.length > 0)
			lower_portion_view.removeAllChildren();
		tableview.setData(tableData);
		lower_portion_view.add(tableview);
		break;
	case 'area':
		if (lower_portion_view.children.length > 0)
			lower_portion_view.removeAllChildren();
		tableview_area.setData(tableData);
		lower_portion_view.add(tableview_area);
		break;
	case 'address':
		if (lower_portion_view.children.length > 0)
			lower_portion_view.removeAllChildren();
		tableview_address.setData(tableData);
		lower_portion_view.add(tableview_address);
		break;
	default:
		break;
	}
}


setDataForBank(banks[0],0);

window.add(upper_portion_view);
window.add(middle_portion_view);
window.add(lower_portion_view);

window.addEventListener("open", function() {
    if (Ti.Platform.osname === "android") {
        if (! window.activity) {
            Ti.API.error("Can't access action bar on a lightweight window.");
        } else {
            actionBar = window.activity.actionBar;
            if (actionBar) {
                actionBar.hide();
                };
            }
        }
    });

controller.open(window);
